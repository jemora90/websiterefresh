<?php
session_start();
require '../funcs/conexion.php';
include '../funcs/funcs.php';

if(!isset($_SESSION["id_usuario"])){ //Si no ha iniciado sesión redirecciona a index.php
	header("Location: ../index.php");
}

$idUsuario = $_SESSION['id_usuario'];

$sql = "SELECT id, nombre FROM usuarios WHERE id = '$idUsuario'";
$result = $mysqli->query($sql);

$row = $result->fetch_assoc();
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Empresa</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"> </script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"> </script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css"> </script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"> </script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css"> </script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"> </script>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="../estilos/css/bootstrap.min.css">
	<script src="../estilos/js/jquery-3.3.1.min.js"></script>
	<script src="../estilos/js/popper.min.js" ></script>
	<script src="../estilos/js/bootstrap.min.js" ></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>


<body>

		<nav style="background-color: white;" class="navbar navbar-expand-lg navbar-default bg-default">
		<div class="navbar-header">
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>
		  </div>
		 <a style="padding-top: 0px;" class="navbar-brand" href="../index.php">
		  <img src="../img/logo.png" style="width: 100px; height:50px; " class="d-inline-block align-top" alt=""/>
        </a>
		  <div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
			 <li class="nav-item dropdown">
			 <?php if($_SESSION['tipo_usuario']==1) { ?>
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				  Administracion
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				  <a class="dropdown-item" href="clientes.php">Clientes</a>
				  <a class="dropdown-item" href="usuarios.php">Usuarios</a>
				  <a class="dropdown-item" href="perfiles.php">Perfiles</a>
				  <a class="dropdown-item" href="permisos.php">Permisos</a>
				</div>
			  </li>
			  <?php } ?>
			  <li class="nav-item dropdown">
			  <?php if($_SESSION['tipo_usuario']==1 || $_SESSION['tipo_usuario']==2) { ?>
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				  Operacion
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				  <a class="dropdown-item" href="test.php">Tests</a>
				</div>
			  </li>
			  <?php } ?>
			</ul>
			<ul class='nav navbar-nav navbar-right'>
						<li><a href='logout.php'>Cerrar Sesi&oacute;n</a></li>
					</ul>
		  </div>
		</nav>
        <h1 style="text-align: center; color: #77D3DE;">Test Estrés Funcional </h1>
		<div class="container" style="width: 500px; height: 505px;">
		<canvas id="myChart" width="341" height="341" style="display: block; width: 341px; height: 341px;"></canvas>
		<table class="table">
		   <tbody>
		       <tr>
			      <td ><i class="fa fa-circle" style="color:#FEDDEE"></i></td>
				  <td>Mi relación con el trabajo</td>
				  <td class="text-right">90%</td>
			   </tr>
			   <tr>
			      <td ><i class="fa fa-circle" style="color:#FEB2CC"></i></td>
				  <td>Mi relación con el equipo de trabajo</td>
				  <td class="text-right">30%</td>
			   </tr>
			   <tr>
			      <td ><i class="fa fa-circle" style="color:#FEDDEE"></i></td>
				  <td>Mi relación en la sociedad</td>
				  <td class="text-right">60%</td>
			   </tr>
			   <tr>
			      <td ><i class="fa fa-circle" style="color:#D6B3E9"></i></td>
				  <td>Mi entorno</td>
				  <td class="text-right">30%</td>
			   </tr>
			   <tr>
			      <td ><i class="fa fa-circle" style="color:#B3EEFF"></i></td>
				  <td>Mi relación conmigo</td>
				  <td class="text-right">15%</td>
			   </tr>
			   <tr>
			      <td ><i class="fa fa-circle" style="color:#77D3DE"></i></td>
				  <td>Mi relación en familia</td>
				  <td class="text-right">25%</td>
			   </tr>
			   <tr>
			      <td ><i class="fa fa-circle" style="color:#BAEEA1"></i></td>
				  <td>Mi relación con mi pareja</td>
				  <td class="text-right">87%</td>
			   </tr>
		   </tbody>
		</table>
		</div>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'pie',
    
    

    // The data for our dataset
    data: {
        labels: ['%Estrés', '%Plenitud'	],
        datasets: [{
            label: 'DATASET A',
            backgroundColor: ["#FEDDEE","#ddd"],
            data: [90, 10],
			
        },
		{
            label: 'DATASET B',
            backgroundColor: ["#FEB2CC","#ddd"],
            data: [30, 70],
			
        },
		{
            label: 'DATASET C',
            backgroundColor: ["#FEDDEE","#ddd"],
            data: [60, 40],
			
        },
		{
            label: 'DATASET D',
            backgroundColor: ["#D6B3E9","#ddd"],
            data: [30, 70],
			
        },
		{
            label: 'DATASET E',
            backgroundColor: ["#B3EEFF","#ddd"],
            data: [15, 85],
			
        },{
            label: 'DATASET F',
            backgroundColor: ["#77D3DE","#ddd"],
            data: [25, 75],
			
        },{
            label: 'DATASET G',
            backgroundColor: ["#BAEEA1","#ddd"],
            data: [87, 13],
			
        }],
		
    },
	
	

    // Configuration options go here
    options: {}
});
</script>

</body>
</html>

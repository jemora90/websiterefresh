<?php
session_start();
require '../funcs/conexion.php';
include '../funcs/funcs.php';

if(!isset($_SESSION["id_usuario"])){ //Si no ha iniciado sesión redirecciona a index.php
	header("Location: ../index.php");
}

$idUsuario = $_SESSION['id_usuario'];

$sql = "SELECT id, nombre FROM usuarios WHERE id = '$idUsuario'";
$result = $mysqli->query($sql);

$row = $result->fetch_assoc();
?>

<html>
<head>
	<title>Resultado</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
	<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js" type="text/javascript"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="../paginas/main.js" ></script>
	<link rel="stylesheet" href="../estilos/css/bootstrap.min.css">
	<script src="../estilos/js/jquery-3.3.1.min.js"></script>
	<script src="../estilos/js/popper.min.js" ></script>
	<script src="../estilos/js/bootstrap.min.js" ></script>
	<script src="../estilos/download/download.min.js" ></script>
	<script src="../estilos/download/download.js" ></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    
</head>

<body>

		<nav style="background-color: white;" class="navbar navbar-expand-lg navbar-default bg-default">
		<div class="navbar-header">
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>
		  </div>
		 <a style="padding-top: 0px;" class="navbar-brand" href="../index.php">
		  <img src="../img/logo.png" style="width: 100px; height:50px; " class="d-inline-block align-top" alt=""/>
        </a>
		  <div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
			 <li class="nav-item dropdown">
			 <?php if($_SESSION['tipo_usuario']==1) { ?>
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				  Administracion
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				  <a class="dropdown-item" href="">Clientes</a>
				  <a class="dropdown-item" href="usuarios.php">Usuarios</a>
				  <a class="dropdown-item" href="perfiles.php">Perfiles</a>
				  <a class="dropdown-item" href="permisos.php">Permisos</a>
				</div>
			  </li>
			  <?php } ?>
			  <li class="nav-item dropdown">
			  <?php if($_SESSION['tipo_usuario']==1) { ?>
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				  Operacion
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				  <a class="dropdown-item" href="test.php">Tests</a>
				</div>
			  </li>
			  <?php } ?>
			</ul>
			<ul class='nav navbar-nav navbar-right'>
						<li><a href='logout.php'>Cerrar Sesi&oacute;n</a></li>
					</ul>
		  </div>
		</nav>

		<div  class="container">
					<script type="text/javascript">
					google.charts.load('current', {'packages':['corechart']});
			  google.charts.setOnLoadCallback(drawChart);

			  function drawChart() {

				var data = google.visualization.arrayToDataTable([
				  ['Etres %', 'Plenitud%'],
				  ['Mi relacion con el trabajo',     11],
				  ['Mi relacion con el equipo de trabajo',      6],
				  ['Mi relacion con la sociedad',  2],
				  ['Mi entorno', 9],
				  ['Mi relacion conmigo',  1],
				  ['Mi relacion en familia', 2],
				  ['Mi relacion con mi pareja',    7]
				]);

				var options = {
				  title: 'Test Estres Funcional -45'
				};

				var chart = new google.visualization.PieChart(document.getElementById('piechart'));	

				chart.draw(data, options);
				
			  }
			</script>
		</div>
            
	   <div id="piechart" style="width: 900px; height: 500px;"></div>
	   <script>
	   document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '">Printable version</a>';
	   </script>
	   <div id='png'></div>
	</body>
	</html>
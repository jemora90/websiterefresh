var defaultThemeColors = Survey
    .StylesManager
    .ThemeColors["default"];
defaultThemeColors["$main-color"] = "#e55976";
defaultThemeColors["$main-hover-color"] = "#db4e6d";
defaultThemeColors["$text-color"] = "#4a4a4a";
defaultThemeColors["$header-color"] = "#e55976";



Survey
    .StylesManager
    .applyTheme();
var json = {
 "locale": "es",
 "title": {
  "default": "Mi relación con el trabajo",
  "es": "TEST ESTRÉS FUNCIONAL"
 },
 "pages": [
  {
   "name": "page1",
   "elements": [
    {
     "type": "matrix",
     "name": "Mi relación con el trabajo",
     "isRequired": true,
     "columns": [
      " Nunca",
      "Casi Nunca",
      "Regularmente",
      "Casi Siempre",
      "Siempre"
     ],
     "rows": [
      "Vivo Estresado y agotado, siento que esto ha sido progresivo",
      "Me sobre exijo para que reconozcan mi trabajo",
      "La prisa es mi compañera y nunca se terminan las actividades",
      "Estoy siempre disponible al teléfono para atender los pendientes y mis asuntos pueden esperar",
      "Creo que lo he dado todo y no me siento realizado",
      "Dejo de comer y/o dormir por los pendientes de la oficina"
     ]
    }
   ]
  },
  {
   "name": "page2",
   "elements": [
    {
     "type": "matrix",
     "name": "Mi relación con el equipo de trabajo",
     "isRequired": true,
     "columns": [
      "Nunca",
      "Casi Nunca",
      "Regularmente",
      "Casi Siempre",
      "Siempre"
     ],
     "rows": [
      "Cuando estoy estresado soy factor de impulso para mi equipo",
      "Cuando doy una indicación, soy claro y se cumple en tiempo y forma",
      "Confío en mi equipo y delego responsabilidades",
      "Me doy tiempo de conocer a mi equipo para conocer su potencial",
      "Mi relación con mis colegas o jefes es colaborativa y genera resultados"
     ]
    }
   ]
  },
  {
   "name": "page3",
   "elements": [
    {
     "type": "matrix",
     "name": "Mi relación en la sociedad",
     "isRequired": true,
     "columns": [
      "Nunca",
      "Casi Nunca",
      "Regularmente",
      "Casi Siempre",
      "Siempre"
     ],
     "rows": [
      "Veo a otros felices, disfrutando  y yo no me siento igual",
      "En el coche voy quejándome de lo mal que manejan los otros",
      "Me quejó del gobierno, mi jefe, mis problemas y son  tema de conversación",
      "Mi vida social ha decaído, ya no salgo como antes, estoy cansado y no tengo tiempo",
      "Voy a las reuniones y no estoy presente, sigo pensando en los pendientes de la oficina",
      "Cuando llego a casa, paso más tiempo aislado viendo televisión que conviviendo"
     ]
    }
   ]
  },
  {
   "name": "page4",
   "elements": [
    {
     "type": "matrix",
     "name": "Mi entorno",
     "isRequired": true,
     "columns": [
      "Nunca",
      "Casi Nunca",
      "Regularmente",
      "Casi Siempre",
      "Siempre"
     ],
     "rows": [
      "Admiro y respeto la naturaleza, cuido la vida de los animales, incluso esos que me desagradan",
      "Tengo hábitos que contribuyen con el cuidado del planeta",
      "Mantengo ordenados y limpios mis cajones y oficina",
      "Cuido del automovil y/o casa, aunque no sean mías"
     ]
    }
   ]
  },
  {
   "name": "page5",
   "elements": [
    {
     "type": "dropdown",
     "name": "Mi relación conmigo: Selecciona las tres emociones que predominan en tu vida:",
     "isRequired": true,
     "validators": [
      {
       "type": "answercount",
       "minCount": 3
      }
     ],
     "choices": [
      "Apatía",
      "Odio",
      "Rabia",
      "Tristeza",
      "Estrés Crónico",
      "Resentimiento",
      "Frustración",
      "Ansiedad",
      "Irritabilidad",
      "Agresividad",
      "Desesperación",
      "Alegría",
      "Amor",
      "Pasión",
      "Estrés Funcional",
      "Paz"
     ]
    },
    {
     "type": "dropdown",
     "name": "question2",
     "isRequired": true,
     "validators": [
      {
       "type": "answercount",
       "minCount": 3
      }
     ],
     "titleLocation": "hidden",
     "choices": [
      "Apatía",
      "Odio",
      "Rabia",
      "Tristeza",
      "Estrés Crónico",
      "Resentimiento",
      "Frustración",
      "Ansiedad",
      "Irritabilidad",
      "Agresividad",
      "Desesperación",
      "Alegría",
      "Amor",
      "Pasión",
      "Estrés Funcional",
      "Paz"
     ]
    },
    {
     "type": "dropdown",
     "name": "question1",
     "isRequired": true,
     "validators": [
      {
       "type": "answercount",
       "minCount": 3
      }
     ],
     "titleLocation": "hidden",
     "choices": [
      "Apatía",
      "Odio",
      "Rabia",
      "Tristeza",
      "Estrés Crónico",
      "Resentimiento",
      "Frustración",
      "Ansiedad",
      "Irritabilidad",
      "Agresividad",
      "Desesperación",
      "Alegría",
      "Amor",
      "Pasión",
      "Estrés Funcional",
      "Paz"
     ]
    },
    {
     "type": "matrix",
     "name": "Mi relación conmigo",
     "isRequired": true,
     "titleLocation": "hidden",
     "columns": [
      "Nunca",
      "Casi Nunca",
      "Regularmente",
      "Casi Siempre",
      "Siempre"
     ],
     "rows": [
      "Hay emociones que se han ido atorando en mi y no se cómo sacarlas",
      "Mi cuerpo se siente tenso todo el día",
      "Tengo un dolor crónico en alguna parte de mi cuerpo",
      "Padezco de problemas digestivos",
      "Padezco de alguna alergía o dermatitis",
      "Ha variado mi peso considerablemente en los últimos 3 años",
      "Siento una presión en el pecho y me falta el aire",
      "He dejado de hacer ejercicio por falta de tiempo y energía",
      "He dejado de tener buenos hábitos alimenticios"
     ]
    }
   ]
  },
  {
   "name": "page6",
   "elements": [
    {
     "type": "matrix",
     "name": "Mi relacion conmigo",
     "isRequired": true,
     "columns": [
      "Nunca",
      "Casi Nunca",
      "Regularmente",
      "Casi Siempre",
      "Siempre"
     ],
     "rows": [
      "A pesar de que duermo me levanto cansado y sin energía",
      "Mi satisfacción sexual ha decaído con el tiempo",
      "Al terminar el día me siento insatisfecho y mi energía está decaída",
      "Me preocupan las noticias del mundo y lo que veo en las redes sociales, esto afecta mi energía durante el día",
      "Cuando me siento tenso, busco alternativas no saludable para relajarme (fumar, tomar, dejar de comer, comer por impulso etc…) ",
      "Constantemente siento que voy a explotar",
      "Siento que el dinero no me rinde y me preocupa",
      "Por sentirme saturado evado o postergo cosas importantes"
     ]
    }
   ]
  },
  {
   "name": "page7",
   "elements": [
    {
     "type": "matrix",
     "name": "Mi relación en familia",
     "isRequired": true,
     "columns": [
      "Nunca",
      "Casi Nunca",
      "Regularmente",
      "Casi Siempre",
      "Siempre"
     ],
     "rows": [
      "Siento que me falta tiempo para estar en familia",
      "Me refugio de mis problemas personales en el trabajo",
      "Tengo una situación deteriorada en mi familia que me preocupa",
      "Me pierdo eventos importantes en familia por darle prioridad al trabajo",
      "Mi cansancio  y falta de energía no me permite convivir con mi familia"
     ]
    }
   ]
  },
  {
   "name": "page8",
   "elements": [
    {
     "type": "radiogroup",
     "name": "Tienes pareja?",
     "isRequired": true,
     "choices": [
      "Si",
      "No"
     ]
    },
    {
     "type": "matrix",
     "name": "Mi relación con mi pareja",
     "visibleIf": "{Tienes pareja?} = \"Si\"",
     "isRequired": true,
     "columns": [
      "Nunca",
      "Casi Nunca",
      "Regularmente",
      "Casi Siempre",
      "Siempre"
     ],
     "rows": [
      "Me siento satisfecho con mi pareja",
      "La comunicación y pasión van en aumento",
      "Intimamente me siento complementado",
      "Me siento amado por mi pareja"
     ]
    },
    {
     "type": "matrix",
     "name": "Mi relación con mi pareja.",
     "visibleIf": "{Tienes pareja?} = \"No\"",
     "valueName": "Mi relación con mi pareja",
     "columns": [
      "Nunca",
      "Casi Nunca",
      "Regularmente",
      "Casi Siempre",
      "Siempre"
     ],
     "rows": [
      "Pienso que cuando mi negocio este bien me daré el tiempo de buscar una relación estable",
      "Quiero tener un compañero de vida, pero no se han dado las cosas"
     ]
    }
   ]
  }
 ],
 "showProgressBar": "bottom"
};

window.survey = new Survey.Model(json);

survey
    .onComplete
    .add(function (result) {
        document
            .querySelector('#surveyResult')
            .innerHTML = '<a href="../paginas/grafica.php">Click para ver tus resultados</a>';
    });

survey.showProgressBar = 'bottom';

$("#surveyElement").Survey({model: survey});
